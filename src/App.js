import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';
import FileView from './Components/FileViewer/FileView';
import DocView from './Components/DocView/DocView';

function App() {
  return (
    <div className="App">


      <h1>Doc Viewer</h1>
      <DocView />
    </div>
  );
}

export default App;
