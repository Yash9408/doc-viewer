import React, { useState } from 'react';
import FileViewer from "react-file-viewer";
import { Upload, Button, Spin, Col, Row } from 'antd';
import { UploadOutlined, DeleteOutlined } from '@ant-design/icons';
import pdf from "../../Assets/eDoc_194_522.pdf"
import image from "../../Assets/depositphotos_251908834-stock-photo-toothbrush-and-toothpaste.jpg";
import csvfile from "../../Assets/annual-enterprise-survey-2019-financial-year-provisional-csv.csv";
import excel from "../../Assets/New Microsoft Office Excel Worksheet.xlsx"
import doc from "../../Assets/New Microsoft Office Word Document.docx"

const FileView = () => {

    const type = 'jgp';
    const path = image;

    const addImage = (e) => {
        console.log(e)
    }

    const onError = (e) => {
        console.log(e);
    }

    return (
        <div>
            <Upload
                listType="picture"
                method="POST"
                customRequest={addImage}
                showUploadList={false}
            >
                <Button icon={<UploadOutlined />}>Upload</Button>
            </Upload>

            <FileViewer fileType={type} filePath={path} onError={onError} style={{ width: '90%', height: '600px' }} />
        </div>
    );
};

export default FileView;