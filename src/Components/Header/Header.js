import React from 'react';
import { Row, Col } from "antd";

const Header = () => {
    return (<>
        <Row>
            <Col xs={{ span: 13 }} lg={{ span: 4 }} md={{ span: 8 }} xs={{ span: 8 }} style={{ padding: '5px 10px' }}>
                <div style={{ textAlign: 'left', display: 'inline', color: 'white', width: 'auto' }}>
                    <h3>DOCUMENT ID</h3>

                </div>
            </Col>
            <Col xs={{ span: 13 }} lg={{ span: 4 }} md={{ span: 8 }} xs={{ span: 8 }} style={{ padding: '5px 10px' }}>
                <div style={{ textAlign: 'left', display: 'inline', color: 'white', width: 'auto' }}>
                    <h3>DOCUMENT TYPE</h3>
                    <p>BIOMETRIC CARD</p>
                </div>
            </Col>
            <Col xs={{ span: 13 }} lg={{ span: 4 }} md={{ span: 8 }} xs={{ span: 8 }} style={{ padding: '5px 10px' }}>
                <div style={{ textAlign: 'left', display: 'inline', color: 'white', width: 'auto' }}>
                    <h3>VALID TILL</h3>
                    <p>2021-02-02</p>
                </div>
            </Col>
        </Row>
    </>
    );
};

export default Header;