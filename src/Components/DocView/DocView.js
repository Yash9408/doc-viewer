import React from 'react';
import DocViewer, { DocViewerRenderers } from "react-doc-viewer";
import "./styles.css";
import { Row, Col } from "antd";
import Header from "../Header/Header"
import pdf from "../../Assets/eDoc_194_522.pdf"
import image from "../../Assets/depositphotos_251908834-stock-photo-toothbrush-and-toothpaste.jpg";
import csvfile from "../../Assets/annual-enterprise-survey-2019-financial-year-provisional-csv.csv";
import excel from "../../Assets/New Microsoft Office Excel Worksheet.xlsx"
import doc from "../../Assets/New Microsoft Office Word Document.docx"

const DocView = () => {

    const docs = [
        { uri: pdf }, // Local File
    ];

    return (
        <Row justify="center" align="center" >
            <Col span={18}>
                <DocViewer
                    documents={docs}
                    pluginRenderers={DocViewerRenderers}
                    style={{ width: '100%', height: '90vh', backgroundColor: 'grey' }}
                    config={{
                        header: {
                            overrideComponent: Header
                        }
                    }
                    }
                />;
           </Col>
        </Row>

    );
};

export default DocView;